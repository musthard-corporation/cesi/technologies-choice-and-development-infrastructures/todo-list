export interface TodoListInterface {
    id?: number,
    name: string,
    elements?: string[],
}

export class TodoList {
    id: number;
    name: string;
    elements: string[];

    constructor(todolist: TodoListInterface) {
        this.id = todolist.id ?? 0;
        this.name = todolist.name;
        this.elements = todolist.elements ?? [];
    }
}