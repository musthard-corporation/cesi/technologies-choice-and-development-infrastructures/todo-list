import express, { Express, NextFunction, Request, Response } from 'express';
import { TodoList } from './todo-list';
import * as Utils from './utils';

let app: Express = express();
app.use(express.urlencoded({ extended: true }));
app.set('view engine', 'pug');

app.use('/public', express.static('public'));

app.get('/', (request: Request, response: Response) => response.redirect('/todolists'));

app.get('/todolists', (request: Request, response: Response) => {
    let todolists = Utils.getTodolists();
    response.render('todo-lists', { todolists });
});

app.get('/todolist', (request: Request, response: Response) => {
    response.render('todo-list-form', { todolist: undefined });
});

app.get('/todolist/:id', (request: Request, response: Response, next: NextFunction) => {
    const id = parseInt(request.params.id, 10);

    if (isNaN(id) || id <= 0) {
        next(`Param id is not a positive number : ${id}`);
    } else {
        const todolist = Utils.getTodolist(id);
        response.render('todo-list-form', { todolist });
    }
});

app.post('/todolist', (request: Request, response: Response) => {
    const { name, elements } = request.body;
    const todolist = Utils.addTodolist(name, elements);
    response.redirect(`/todolist/${todolist.id}`);
});

app.post('/todolist/:id', (request: Request, response: Response, next: NextFunction) => {
    const id = parseInt(request.params.id, 10);

    if (isNaN(id) || id <= 0) {
        next(`Param id is not a positive number : ${id}`);
    } else {
        const { name, elements } = request.body;
        Utils.updateTodolist(new TodoList({ id, name, elements }));
        response.redirect(`/todolist/${id}`);
    }
});

if (process.env.NODE_ENV === 'production') {
    app.listen(5000, () => console.log(`Listening on port 5000`));
}

export const viteNodeApp = app;
