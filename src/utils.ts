import fs from 'fs';
import { TodoList, TodoListInterface } from "./todo-list";
import path from 'path';

const filepath = './resources/todolists.json';

export function addTodolist(name: string, elements: string[]): TodoList {
    let todolists = getTodolists();
    const id = Math.max(...todolists.map(t => t.id), 0) + 1;
    const todolist = new TodoList({ id, name, elements });
    todolists.push(todolist);
    fs.writeFileSync(filepath, JSON.stringify(todolists));
    return todolist;
}

export function updateTodolist(todolist: TodoList): void {
    let todolists = getTodolists();
    const index = todolists.findIndex(t => t.id === todolist.id);
    if (index > -1) {
        todolists[index] = todolist;
    }
    fs.writeFileSync(filepath, JSON.stringify(todolists));
}

export function getTodolists(): TodoList[] {
    const dir = path.dirname(filepath);
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir, { recursive: true });
    }

    if (fs.existsSync(filepath)) {
        let content: TodoListInterface[] = JSON.parse(fs.readFileSync(filepath, 'utf-8'));
        return content.map(c => new TodoList(c));
    } else {
        return [];
    }
}

export function getTodolist(id: number): TodoList | undefined {
    return getTodolists().find(t => t.id === id);
}
